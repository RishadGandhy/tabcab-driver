package in.co.datavoice.tabcabdriver.utils;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by konsultera on 13/11/17.
 */

public class SendGpsData {

    private String header, packetType, imei,gps, taxiNo, date,time,tripId,tripDate,tripTime,waitTime,TripEndDate,distance;
    private String tripEndTime, tripEndDate, fare,serviceTax,amount,SoftwareVersion,checksum,dataPacket;
    private String latitude, latitudeDirection, longitude, longitudeDirection, speed, bearing, status;
    private String doorAlarm, panicAlarm,battery,batteryAlert, batteryType,signalStrength,rateMode,ack,rateModeAmount;
    private String commandString;
    private String TAG = "SendGpsData";
    private String totalTripTime;

    public SendGpsData(String softwareVersion) // initiate the parameters
    {
        this.header ="!Sansui";
        this.packetType = "LTIMED";
        this.gps = "G";
        this.status = "H";
        this.battery = "50";
        this.batteryType = "N";
        this.doorAlarm = "N";
        this.panicAlarm = "N";
        this.batteryAlert = "S";
        this.signalStrength = "23";
        this.rateMode = "1";
        this.ack = "N";
        rateModeAmount = "00.00";
        this.SoftwareVersion = softwareVersion;//"SW DATAV V4.00.15C.AH.S";
        this.checksum = "01";
        this.dataPacket = "38565";
        this.tripId = "0";
        totalTripTime = "0";
    }
    public void setTripData(String imei, String taxiNo, String tripId, String tripDate)
    {
        String currentDate = "", currentTime = "", TripDate = "", TripTime = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");

            currentDate = sdf.format(new Date());
            currentTime = sdf2.format(new Date());

            TripDate = sdf.format(sdf.parse(tripDate));

            TripTime = null;

            TripTime = sdf2.format(sdf.parse(tripDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.imei = imei;
        this.taxiNo = taxiNo;
        this.date = currentDate;
        this.time = currentTime;
        this.tripId = tripId;
        this.tripDate = TripDate;
        this.tripTime = TripTime;
    }
    public void setTripData(String imei, String taxiNo)
    {
        String currentDate = "", currentTime = "", TripDate = "", TripTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");

        currentDate = sdf.format(new Date());
        currentTime = sdf2.format(new Date());
        this.imei = imei;
        this.taxiNo = taxiNo;
        this.date = currentDate;
        this.time = currentTime;
    }
    public String getImei(){return this.imei;}
    public void setImei(String imei){this.imei = imei;}



    public void setLocation(Location location)
    {
        this.latitude = location.getLatitude() + "";
        this.longitude = location.getLongitude() + "";
        if(location.hasBearing()) {
            float currentBearing = location.getBearing();
            if(currentBearing <= 180)
            {
                this.latitudeDirection = "N";
            }
            else
            {
                this.latitudeDirection = "S";
            }
            if(currentBearing <= 90 || currentBearing >=270 )
            {
                this.longitudeDirection = "E";
            }
            else
            {
                this.longitudeDirection= "W";
            }
            this.bearing = currentBearing + "";
        }
        this.speed = location.getSpeed() / 1000 + "";
    }
    public String getCommandString()
    {
        return this.commandString;
    }

    public void createTripString()
    {
        commandString = header + "," + packetType + "," + imei + "," + taxiNo + ","
                + gps + ","+ date + "," + time + "," + latitude + "," + latitudeDirection + ","
                + longitude +"," + longitudeDirection + "," + speed + "," + bearing + "," + status
                + ","+doorAlarm + "," + panicAlarm + "," + battery + "," + batteryAlert + "," +
                batteryType + "," + signalStrength + "," + "0," + rateMode + "," + ack + ","+
                rateModeAmount + "," + SoftwareVersion + "," + checksum + "," + dataPacket;
    }



    public void UpdateDateTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = sdf.format(new Date());
        sdf = new SimpleDateFormat("HH:mm:ss");
        String currentTime = sdf.format(new Date());
        this.date = currentDate;
        this.time = currentTime;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }


    public void sendGpsData(final Context context) {
        new AsyncTask<Void, Void, JSONObject>() { // create a new thread to process


            @Override
            protected void onPreExecute() {

            }

            @Override
            protected JSONObject doInBackground(Void... params) { // what to do in the thread
                MyJSON test = new MyJSON(context);
                try {
                    String Json1 = test.CreateSendGpsData(header,packetType,imei,taxiNo, gps, date, time, latitude,
                            latitudeDirection, longitude, longitudeDirection, speed, bearing, status, doorAlarm,
                            panicAlarm, battery, batteryAlert, batteryType, signalStrength, "0", rateMode, ack,
                            rateModeAmount, SoftwareVersion, checksum, dataPacket,tripId,distance,totalTripTime);
                    HttpResponse Json2 = test.SendJSON("/updateCabLocation.php", Json1);
                    try { // convert the response into a JSON object
                        JSONObject Json3 = test.GetJson(Json2);
                        Log.v(TAG, Json3.toString());
                        return Json3;

                    }catch (NullPointerException e){
                        Log.v(TAG, e.toString());
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return new JSONObject();
            }

            @Override
            protected void onPostExecute(JSONObject response) {
                // mobile.setText(response);
                // JSONData = response;
                Intent intent;
                try {
                    JSONObject responsedata = response;
                    Log.v(": ", response.toString());

                    if(responsedata.getString("data").equalsIgnoreCase("true") ) // if there are children
                    {
                        Log.v(TAG,responsedata.toString());


                    }
                    else
                    {
                        Log.v(TAG, "error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    public void setDistance(double distance) {
        this.distance = String.valueOf(distance);
    }

    public void setAmount(double finalAmount) {
        this.rateModeAmount = String.valueOf(finalAmount);
    }

    public void setTripTime(String totalTripTime) {
        this.totalTripTime = totalTripTime;
    }
}
