package in.co.datavoice.tabcabdriver.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/*
 * Created by Rishad on 3/26/2015.
 */
public class MyGcm {
    private Context context;
    private String regid;
    private String TAG = "GCM";
    private String SenderId = "492687223817";
    private String mobno = "";
    private String imei = "";
    private String PROPERTY_APP_VERSION = "1.0";
    private GoogleCloudMessaging gcm;
    public void setImei(String imei)
    {
        this.imei = imei;
    }
    public MyGcm(Context context1) {
        this.context = context1;
    }

    public void setMobno(String mob)
    {
        this.mobno = mob;
    }
    private boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                // GooglePlayServicesUtil.getErrorDialog(resultCode, this.context.,
                //       PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    private String RegisterGcm()
    {
        if(checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this.context);
            regid = getRegistrationId();
            return regid;
        }
        else
        {
            return "";
        }
    }



    public String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString("PROPERTY_REG_ID", "");
        if (registrationId.isEmpty()) {
            Log.v(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.v(TAG, "App version changed.");
            return "";
        }
        Log.v(TAG, "reg id is already set: " + registrationId);
        return registrationId;
    }


    private SharedPreferences getGCMPreferences() {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the registration ID in your app is up to you.

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.context);
        return prefs;

    }
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    public void registerInBackground() {
        new AsyncTask() {
            @Override
            protected String doInBackground(Object[] objects) {
                String msg = "";
                Log.v(TAG,"in registerinbackground");
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SenderId);
                        msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the registration ID - no need to register again.
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                Log.v(TAG, msg);
                return msg;

            }
        }.execute(null, null, null);

        }

    public void sendRegistrationIdToBackend() {
        new AsyncTask<Void, Void, JSONObject>() {
            @Override
            protected JSONObject doInBackground(Void... params) {
                try {

                    MyJSON myjson = new MyJSON(context);
                    Log.v(TAG, "mobno = " + mobno + "; regid = " + regid);
                    String Json1 = myjson.CreateJSON2(mobno, regid, imei);
                    myjson.SendJSON("update_GCM.php", Json1);
                    HttpResponse Json2 = myjson.SendJSON("/update_GCM.php", Json1);

                    Log.v(TAG, "REGISTER ONLINE= ");
                    Log.v(TAG, "response = " + Json2.toString());
                    JSONObject Json3 = myjson.GetJson(Json2);
                    Log.v(TAG, "JSON3 COMPLETED= " + Json3);

                } catch (NullPointerException e) {
                    Log.v(TAG, e.toString());
                } catch (JSONException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                return new JSONObject();
            }
        }.execute(null,null,null);

    }
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersion = getAppVersion(context);
        Log.v(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("PROPERTY_REG_ID", regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }



}
