package in.co.datavoice.tabcabdriver.utils;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import in.co.datavoice.tabcabdriver.R;

/**
 * Created by Rishad on 05-May-16.
 */
public class MapHelper {
    Marker cab = null;
    private GoogleMap map;
    Location location;
    public MapHelper(GoogleMap map)
    {
        this.map = map;

    }

    public void AddPointer(LatLng latLng)
    {
        map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory
                .fromResource(R.drawable.user_icon)));

    }
    public void updateLocation(Location location)
    {

        LatLng CameraLocation = new LatLng(location.getLatitude(),location.getLongitude());
        if(cab == null) {
            cab = map.addMarker(new MarkerOptions().position(CameraLocation).icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.shaline_car)));
        }
        else
        {
            cab.setPosition(new LatLng(location.getLatitude(),location.getLongitude()));
            cab.setRotation(location.getBearing());
            float asd  = location.getBearing();
            Log.v("marker","updated");
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(CameraLocation)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                .build();                   // Creates a CameraPosition from the builder
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }
}
