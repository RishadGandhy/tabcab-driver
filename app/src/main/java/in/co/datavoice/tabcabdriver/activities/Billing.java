package in.co.datavoice.tabcabdriver.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import in.co.datavoice.tabcabdriver.R;
import in.co.datavoice.tabcabdriver.utils.SendGpsData;

public class Billing extends AppCompatActivity implements View.OnClickListener, LocationListener {
    LocationManager locationManager;

    TextView tvjobno1, tvcustomername1, tvtraveldistance1, tvtraveltime, tvbill1, tvtotal1, tvtax;
    Button btnprint1;
    Timer Ttimer;
    private SharedPreferences prefs;
    private SendGpsData sendGpsData;
    private TimerTask tt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


        tvjobno1 = findViewById(R.id.jobno);
        tvcustomername1 = findViewById(R.id.customername);
        tvtraveldistance1 = findViewById(R.id.traveldistanceamount);
        tvtraveltime = findViewById(R.id.traveltimeamount);
        tvtax = findViewById(R.id.tax);

        tvbill1 = findViewById(R.id.billamount);
        tvtotal1 = findViewById(R.id.totalpayable);
        btnprint1 = findViewById(R.id.print);
        Ttimer = new Timer();

        btnprint1.setOnClickListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000,   // 1 sec
                0,  // 20 meter
                this);

        PackageInfo pInfo = null;
        String version = "";
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        final String imei = telephonyManager.getDeviceId();
        sendGpsData = new SendGpsData(version);
        Ttimer.scheduleAtFixedRate(tt = new TimerTask() {
            @Override
            public void run() {
                // if the trip has started and the cab is not moving
                try {

                    sendGpsData.UpdateDateTime();
                    sendGpsData.setTripData(imei, prefs.getString("cabNumber", ""));
                    sendGpsData.setStatus("P");
                    sendGpsData.setImei(imei);
                    sendGpsData.createTripString();
                    /*PrintWriter out;
                    out = new PrintWriter(new BufferedWriter(
                            new OutputStreamWriter(socket.getOutputStream())),
                            true);
                    out.println(str);*/
                    sendGpsData.sendGpsData(getApplicationContext());


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 5000);//put here time 1000 milliseconds=1 second


        Bundle extras = getIntent().getExtras();
        setData(extras);

    }

    public void setData(Bundle extras) {


        double bill = extras.getDouble("totalBill");
        double tax = Math.round(extras.getDouble("amountWithTax")) - bill;

        tvjobno1.setText("Job No.\n" + prefs.getString("trip_id", "0"));
        tvcustomername1.setText(prefs.getString("cust_name", "No Name"));
        tvtraveldistance1.setText("Distance\n" + extras.getString("distance") + " Km");
        tvtraveltime.setText("Trip Time\n" + extras.getString("tripTime"));

        tvtax.setText("Tax\nRs. " + String.valueOf(Math.round(tax)));
        tvbill1.setText("Fare\nRs. " + String.valueOf(Math.round(bill)));
        tvtotal1.setText("Rs. " + String.valueOf(Math.round(extras.getDouble("amountWithTax"))));
    }

    @Override
    public void onClick(View v) {
        prefs.edit().putString("date", "").apply();
        prefs.edit().putFloat("distance", 0).apply();
        prefs.edit().putBoolean("TripStarted", false).apply();
        prefs.edit().putLong("waitTime", 0).apply();
        prefs.edit().putLong("StartTime", 0).apply();
        prefs.edit().putString("startSpeedometer", "0").apply();
        prefs.edit().putString("strStartupTime", "").apply();
        Intent i = new Intent(getBaseContext(), Waiting.class);
        tt.cancel();
        Ttimer.cancel();
        Ttimer.purge();

        startActivity(i);
        //finish();
    }

    @Override
    public void onBackPressed() {
        //moveTaskToBack(true);
        Toast.makeText(getBaseContext(), "Cannot go back", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onLocationChanged(Location location) {
        sendGpsData.setLocation(location);

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000,   // 1 sec
                0,  // 20 meter
                this);
    }
}
