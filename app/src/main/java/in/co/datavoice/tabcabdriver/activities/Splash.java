package in.co.datavoice.tabcabdriver.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import in.co.datavoice.tabcabdriver.R;
import in.co.datavoice.tabcabdriver.utils.RequestHandler;

public class Splash extends AppCompatActivity {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();
        getLink();
        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                RequestHandler rh = new RequestHandler();
                String result = rh.sendPostRequest("http://139.162.32.211/apna_deal_sir/webservice/getApnaDealSirLink.php");
                if(result.equalsIgnoreCase("Error Registering")){
                    result = rh.sendPostRequest("http://139.162.32.211/apna_deal_sir/webservice/getApnaDealSirLink.php");
                }
                Log.v("Splash",result);
                editor.putString("url",result);
                editor.apply();
                Intent i = new Intent(getBaseContext(), Splash.class);
                startActivity(i);
                finish();

            }
        }, 5000);*/
    }

    private void getLink() {
        class getLink extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //Log.v("done", s);

                Intent i = new Intent(getBaseContext(), Login.class);
                startActivity(i);
                finish();

            }

            @Override
            protected String doInBackground(Void... params) {

                RequestHandler rh = new RequestHandler();
                String result = rh.sendPostRequest("http://139.162.32.211/app_links/getTabcabDriverLinkTest.php");
                Log.v("Splash","1");
                Log.v("Splash",result);
                if(result.equals("Error Registering")){
                    result = rh.sendPostRequest("http://188.138.11.5/app_links/getTabcabDriverLinkTest.php");
                    Log.v("Splash","2");
                    Log.v("Splash",result);
                }
                try {
                    JSONObject result2 = new JSONObject(result);
                    Log.v("Splash", String.valueOf(result2));
                    editor.putString("url",result2.getString("data"));
                    editor.apply();
                    return result;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }



            }
        }
        getLink gl = new getLink();
        gl.execute();

    }
}
