package in.co.datavoice.tabcabdriver.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import in.co.datavoice.tabcabdriver.R;
import in.co.datavoice.tabcabdriver.services.SyncService;
import in.co.datavoice.tabcabdriver.utils.MyJSON;


public class Login extends AppCompatActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 12315;
    EditText username, password;
    String TAG = "Login", imei;
    SharedPreferences prefs;
    CheckBox remember;
    Button login;
    TextView appversion;
    String driverName, driverNumber, driverId, cabNumber;
    int PERMISSIONS_REQUEST_READ_PHONE_STATE = 45;
    SharedPreferences.Editor editor;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_PHONE_STATE,Manifest.permission.WAKE_LOCK,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.VIBRATE},
                MY_PERMISSIONS_REQUEST_READ_CONTACTS);*/

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        Intent serviceIntent = new Intent(getApplicationContext(), SyncService.class);
        getApplicationContext().startService(serviceIntent);
        if (prefs.getBoolean("TripStarted", false)) {
            Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
            startActivity(intent);
            //finish();
        }

        editor = prefs.edit();

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_PHONE_STATE,Manifest.permission.WAKE_LOCK,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.VIBRATE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                imei = telephonyManager.getImei();
            } else {
                imei = telephonyManager.getDeviceId();
            }
        }

        username = findViewById(R.id.editText);
        password = findViewById(R.id.editText2);
        remember = findViewById(R.id.remember);
        login = findViewById(R.id.login);
        password.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    getLogin();
                    return true;
                }
                return false;
            }
        });
        appversion = findViewById(R.id.version);
        login.setOnClickListener(this);
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            appversion.setText("Current App Version:  " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if(prefs.getBoolean("remember",false))
        {
            username.setText(prefs.getString("username",""));
            password.setText(prefs.getString("password",""));
            remember.setChecked(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_PHONE_STATE,Manifest.permission.WAKE_LOCK,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.VIBRATE},
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                        return;
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        imei = telephonyManager.getImei();
                    } else {
                        imei = telephonyManager.getDeviceId();
                    }

                } else {
                    Toast.makeText(this, "Please grant the permissions.", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_PHONE_STATE,Manifest.permission.WAKE_LOCK,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.VIBRATE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                }
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.update) {
            Log.v(TAG,"opening store");
            final String appPackageName = getPackageName();
            Log.v(TAG,appPackageName);// getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

    @Override
    protected void onResume() {
        super.onResume();
        /*WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|

                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (50 * getResources()
                .getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        customViewGroup view = new customViewGroup(this);

        manager.addView(view, localLayoutParams);*/


    }

    @SuppressLint("StaticFieldLeak")
    private void getLogin() {
    new AsyncTask<Void, Void, JSONObject>() { // create a new thread to process
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(Login.this,"Loading", "Please wait");
        }

        @Override
        protected JSONObject doInBackground(Void... params) { // what to do in the thread
            MyJSON test = new MyJSON(getApplicationContext());
            try {
                String Json1 = test.CreateJSONLogin(username.getText().toString(),password.getText().toString(),imei);
                Log.v(TAG,"sending:" + Json1);
                HttpResponse Json2 = test.SendJSON("/login.php", Json1);
                try { // convert the response into a JSON object
                    JSONObject Json3 = test.GetJson(Json2);
                    Log.v(TAG, Json3.toString());
                    return Json3;

                }catch (NullPointerException e){
                    Log.v(TAG, e.toString());
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            // mobile.setText(response);
            // JSONData = response;
            Intent intent;
            try {
                JSONObject responsedata = response;
                Log.v("the response is: ", response.toString());

                if(responsedata.getString("data").equalsIgnoreCase("true") ) // if there are children
                {
                    Log.v(TAG,responsedata.toString());
                    editor.putBoolean("login",true);
                    intent = new Intent(getApplicationContext(),Waiting.class);
                    editor.putString("driverName",responsedata.getString("driver_name"));
                    editor.putString("driverNumber",responsedata.getString("driver_number"));
                    editor.putString("cabNumber",responsedata.getString("cab_number"));
                    editor.putString("cab_id",responsedata.getString("cab_id"));

                    editor.apply();
                    if(remember.isChecked()) {
                        editor.putString("username",username.getText().toString());
                        editor.putString("password",password.getText().toString());
                        editor.putBoolean("remember",true);
                        editor.apply();
                    }
                    else
                    {
                        editor.putString("username","");
                        editor.putString("password","");
                        editor.putBoolean("remember",false);
                        editor.apply();
                    }
                    dialog.dismiss();
                    Toast.makeText(getBaseContext(), responsedata.getString("msg"), Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                    //finish();
                }
                else
                {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), responsedata.getString("msg"), Toast.LENGTH_LONG).show();
                    Log.v(TAG, "error");
                }
            } catch (JSONException e) {
                editor.putString("username",username.getText().toString());
                editor.putString("password","");
                editor.putBoolean("remember",false);
                editor.commit();
                e.printStackTrace();
                dialog.dismiss();
            }
        }
    }.execute(null, null, null);
}

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login)
        {
            getLogin();
        }
    }

    @Override
    public void onBackPressed() {
        //moveTaskToBack(true);
        //Toast.makeText(getBaseContext(),"Cannot go back",Toast.LENGTH_SHORT).show();
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Login.this.finishAffinity();

                    }
                })
                .setNegativeButton("No", null)
                .show();
    }


}
