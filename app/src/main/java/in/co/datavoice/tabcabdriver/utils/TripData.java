package in.co.datavoice.tabcabdriver.utils;

/**
 * Created by Admin on 01-02-2018.
 */

public class TripData {

    String trip_id,json,url;

    public TripData(String trip_id, String json, String url) {
        this.trip_id = trip_id;
        this.json = json;
        this.url = url;
    }

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
