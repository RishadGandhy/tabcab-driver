package in.co.datavoice.tabcabdriver.utils;

import android.location.Location;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Created by Rishad on 6/23/2015.
 */
public class TaxiData {

    private String header, packetType, imei,gps, taxiNo, date,time,tripId,tripDate,tripTime,waitTime,TripEndDate;
    private String tripEndTime, tripEndDate, fare,serviceTax,amount,SoftwareVersion,checksum,dataPacket;
    private String latitude, latitudeDirection, longitude, longitudeDirection, speed, bearing, status;
    private String doorAlarm, panicAlarm,battery,batteryAlert, batteryType,signalStrength,rateMode,ack,rateModeAmount;
    private String commandString;
    private double distance;

    public TaxiData() // initiate the parameters
    {
        this.header ="!Sansui";
        this.packetType = "LTIMED";
        this.gps = "G";
        this.status = "H";
        this.battery = "50";
        this.batteryType = "N";
        this.doorAlarm = "N";
        this.panicAlarm = "N";
        this.batteryAlert = "S";
        this.signalStrength = "23";
        this.rateMode = "1";
        this.ack = "N";
        rateModeAmount = "100.00";
        this.SoftwareVersion = "SW DATAV V4.00.15C.AH.S";
        this.checksum = "01";
        this.dataPacket = "38565";
    }
    public void setTripData(String imei, String taxiNo, String tripId, String tripDate)
    {
        String currentDate = "", currentTime = "", TripDate = "", TripTime = "";
        try {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");

         currentDate = sdf.format(new Date());
         currentTime = sdf2.format(new Date());

        TripDate = sdf.format(sdf.parse(tripDate));

        TripTime = null;

        TripTime = sdf2.format(sdf.parse(tripDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.imei = imei;
        this.taxiNo = taxiNo;
        this.date = currentDate;
        this.time = currentTime;
        this.tripId = tripId;
        this.tripDate = TripDate;
        this.tripTime = TripTime;
    }
    public void setTripData(String imei, String taxiNo)
    {
        String currentDate = "", currentTime = "", TripDate = "", TripTime = "";
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");

            currentDate = sdf.format(new Date());
            currentTime = sdf2.format(new Date());
        this.imei = imei;
        this.taxiNo = taxiNo;
        this.date = currentDate;
        this.time = currentTime;
    }
    public String getImei(){return this.imei;}
    public void setImei(String imei){this.imei = imei;}



    public void setLocation(Location location)
    {
        this.latitude = location.getLatitude() + "";
        this.longitude = location.getLongitude() + "";
        if(location.hasBearing()) {
            float currentBearing = location.getBearing();
            if(currentBearing <= 180)
            {
               this.latitudeDirection = "N";
            }
            else
            {
                this.latitudeDirection = "S";
            }
            if(currentBearing <= 90 || currentBearing >=270 )
            {
                this.longitudeDirection = "E";
            }
            else
            {
                this.longitudeDirection= "W";
            }
            this.bearing = currentBearing + "";
        }
        this.speed = location.getSpeed() / 1000 + "";
    }
    public String getCommandString()
    {
        return this.commandString;
    }

    public void createTripString()
    {
        commandString = header + "," + packetType + "," + imei + "," + taxiNo + ","
                + gps + ","+ date + "," + time + "," + latitude + "," + latitudeDirection + ","
                + longitude +"," + longitudeDirection + "," + speed + "," + bearing + "," + status
                + ","+doorAlarm + "," + panicAlarm + "," + battery + "," + batteryAlert + "," +
                    batteryType + "," + signalStrength + "," + "0," + rateMode + "," + ack + ","+
                rateModeAmount + "," + SoftwareVersion + "," + checksum + "," + dataPacket;
    }
    public void createTripEndString()
    {
        commandString = this.header + "," + this.packetType + "," + imei + "," + taxiNo + "," + date + "," +
                time + "," + waitTime + "," + tripEndDate + "," + tripEndTime+ "," +distance+ "," +
                    fare+ "," +serviceTax+ "," +amount+ "," +this.SoftwareVersion+ "," +this.checksum+ "," +dataPacket;
    }
    public void UpdateDateTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        String currentDate = sdf.format(new Date());
        sdf = new SimpleDateFormat("HHmmss");
        String currentTime = sdf.format(new Date());
        this.date = currentDate;
        this.time = currentTime;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

}
