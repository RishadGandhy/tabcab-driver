package in.co.datavoice.tabcabdriver.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rishad on 2/27/2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    String TAG = "database";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "db_tabcab_airport";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_table = "CREATE TABLE trip_master (id INTEGER PRIMARY KEY AUTOINCREMENT" +
                ", trip_id TEXT, " +
                "date TEXT," +
                "json TEXT," +
                "url TEXT," +
                "updated TEXT )";


        db.execSQL(create_table);



    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    public void addTripJson(String trip_id, String date, String json, String url, String updated) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("trip_id", trip_id); // Trip Id
        values.put("date", date); // Date
        values.put("json", json); // Json
        values.put("url", url); // url to send
        values.put("updated", updated); // status

        db.insert("trip_master", null, values);
        db.close();

    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public void update_trip_update_status(String id, String updated_date) {
        Log.v(TAG, "updating student status in DB " + id + "| " + updated_date);
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "UPDATE trip_master set updated = 'Y',date = '" + updated_date + "' where trip_id='" + id + "'";
        Log.v("DB", "updated student id: " + id + " at time: " + updated_date);
        db.execSQL(sql);
        db.close();
    }
    public TripData[] getUnUpdatedTripData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("trip_master", new String[]{"trip_id", "json", "url"},  "updated=?",
                new String[]{"N"}, null, null, null, null);
        Log.v(TAG,cursor.getCount() + "count");
        TripData[] tripData = new TripData[cursor.getCount()];
        int i = 0;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            tripData[i] = new TripData(cursor.getString(0), cursor.getString(1), cursor.getString(2));
            i++;
            while (cursor.moveToNext()) {
                tripData[i] = new TripData(cursor.getString(0), cursor.getString(1), cursor.getString(2));
                i++;
            }
            cursor.close();
            db.close();
            return tripData;
        }
        return null;
    }
}
