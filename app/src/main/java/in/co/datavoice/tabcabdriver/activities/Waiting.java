package in.co.datavoice.tabcabdriver.activities;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import in.co.datavoice.tabcabdriver.R;
import in.co.datavoice.tabcabdriver.utils.DownloadTask;
import in.co.datavoice.tabcabdriver.utils.MapHelper;
import in.co.datavoice.tabcabdriver.utils.MyGcm;
import in.co.datavoice.tabcabdriver.utils.SendGpsData;


public class Waiting extends AppCompatActivity implements OnMapReadyCallback, LocationListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int SERVERPORT = 8891;
    public static TimerTask tt;
    public static Timer timertt;
    private final String SERVER_IP = "69.64.34.208";
    TextView driverName, cabNumber;
    SharedPreferences prefs;
    Location location1 = null;
    Context context;
    String locationName;
    String driverNumber, ScabNumber;
    String TAG = "Waiting";
    public static LocationManager locationManager;
    ImageView profile;
    DownloadTask downloadTask;
    String command;
    File file;
    String imei;
    SendGpsData sendGpsData;
    String filename;
    Location oldLocation;
    String Lat, Long;
    int mId = 542321;
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;
    NotificationManager mNotificationManager;
    int timer = 0;
    MapHelper mapHelper = null;
    Boolean firstTime = true; // if the app has just started
    private Socket socket;
    private GoogleMap map;
    private boolean logout = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);
        context = this;

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        driverNumber = prefs.getString("driverNumber", "");
        ScabNumber = prefs.getString("cabNumber", "");
        timertt = new Timer();
        //createDirIfNotExists("Shaline");
        //new Thread(new ClientThread()).start();
        // Here, thisActivity is the current activity
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imei = telephonyManager.getDeviceId();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        MyGcm gcm = new MyGcm(this);
        String regid = gcm.getRegistrationId();
        Log.v(TAG, "Reg id: " + regid + "setting mobno = " + ScabNumber);
        gcm.setMobno(ScabNumber);
        gcm.setImei(imei);
        gcm.registerInBackground();
        gcm.sendRegistrationIdToBackend();
        final String TAG = "waiting";
        setUpMapIfNeeded();
        //map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
        //      .getMap();
        mapHelper = new MapHelper(map);
        driverName = findViewById(R.id.driverName);
        cabNumber = findViewById(R.id.cabNumber);
        profile = findViewById(R.id.imageView6);
        setDriver_car();
        PackageInfo pInfo = null;
        String version = "";
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        sendGpsData = new SendGpsData(version);
        sendGpsData.UpdateDateTime();
        sendGpsData.setStatus("F");
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000,   // 1 sec
                0,  // 20 meter
                this);

        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            turnGPSOn();
        }
        try {
            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/tabcab");

            if (!folder.exists()) {
                folder.mkdir();
            }
            filename = prefs.getString("profile_pic", "uploads/pic_not_found.jpg").substring(8);
            downloadTask = new DownloadTask(filename, false);
            downloadTask.execute("http://" + "69.64.34.208" + prefs.getString("profile_pic", ""));
            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/tabcab/" + filename);
            if (!file.exists()) {
                DisplayImage();
            } else {
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                profile.setImageBitmap(yourSelectedImage);
            }
        } catch (Exception e) {
            Log.v(TAG, e.getMessage().toString());
        }
        timertt.scheduleAtFixedRate(tt = new TimerTask() {
            @Override
            public void run() {
                // if the trip has started and the cab is not moving
                if (timer % 2 == 1) {
                    // checkTrip();
                    timer = 1;
                    if (firstTime) {
                        sendGpsData.setStatus("S");
                        firstTime = false;
                        Log.v(TAG, "first in here");
                    } else if(logout){
                        sendGpsData.setStatus("E");
                    }else {
                        sendGpsData.setStatus("F");
                        Log.v(TAG, "not first in here");


                    }
                    sendGpsData.setTripData(imei, prefs.getString("cabNumber", ""));

                    sendGpsData.createTripString();
                    sendGpsData.sendGpsData(getApplicationContext());
                    /*String str = sendGpsData.getCommandString();
                    PrintWriter out = null;
                    try {
                        out = new PrintWriter(new BufferedWriter(
                                new OutputStreamWriter(socket.getOutputStream())),
                                true);
                        Log.v("asd", "tt Running - waiting");
                        Log.v("asd", str + "messaGE");
                        out.println(str);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(), "error: couldnt send data:" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }catch (NullPointerException e) {
                        e.printStackTrace();
                        //new Thread(new ClientThread()).start();
                    }*/

                }
                timer++;

            }
        }, 0, 5000);//put here time 1000 milliseconds=1 second
    }
    private void turnGPSOn(){
        GoogleApiClient googleApiClient;

            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            Log.v(TAG, String.valueOf(status.getStatusCode()));
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                Log.v(TAG, String.valueOf(status.getStatusCode()));
                                status.startResolutionForResult(
                                        Waiting.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            Log.v(TAG, String.valueOf(status.getStatusCode()));
                            break;
                    }
                }
            });
    }
    private void setUpMapIfNeeded() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case 1000:
                switch (resultCode) {
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(context, "Please allow location service to go further.", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getBaseContext(), Login.class);
                        startActivity(i);
                        //finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        //new Thread(new ClientThread()).start();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000,   // 1 sec
                0,  // 20 meter
                this);

    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(Waiting.this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //locationManager.removeUpdates(this);
        //tt.cancel();
        Log.v(TAG, "called onPause");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_waiting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {

            logout = true;
            prefs.edit().putBoolean("login",false).apply();
            sendGpsData.UpdateDateTime();
            sendGpsData.setStatus("E");
            sendGpsData.setTripData(imei, prefs.getString("cabNumber", ""));
            sendGpsData.createTripString();
            String str = sendGpsData.getCommandString();
            PrintWriter out = null;
            sendGpsData.sendGpsData(getApplicationContext());
            /*try {
                out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())),
                        true);
                Log.v("asd", "tt Running - waiting");
                Log.v("asd", str + "messaGE");
                out.println(str);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "error: couldnt send data:" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }catch (NullPointerException e) {
                e.printStackTrace();
            }*/
            Log.v(TAG, "logout:" + str);
            Intent i = new Intent(getBaseContext(), Login.class);
            startActivity(i);
            tt.cancel();
            timertt.cancel();
            timertt.purge();
            //finish();

        }
        /*else if(id == R.id.reports)
        {
            startActivity(new Intent(getBaseContext(), ReportsActivity.class));
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void setDriver_car() {
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String strdriverName = prefs.getString("driverName", "");
        if (strdriverName.length() > 15) {
            strdriverName = strdriverName.substring(0, 14) + "...";
        }
        driverName.setText(strdriverName);
        cabNumber.setText(prefs.getString("cabNumber", ""));
    }

    @Override
    public void onLocationChanged(Location location) {
        mapHelper.updateLocation(location);

        if (location1 == null) // if there is no location set yet
        {
            location1 = location;
            locationName = getLocationName(location.getLatitude(), location.getLongitude());
        } else // if there is already a location
        {
            locationName = getLocationName(location.getLatitude(), location.getLongitude());

        }

        Lat = location.getLatitude() + "";
        Long = location.getLatitude() + "";
        sendGpsData.setLocation(location);
        sendGpsData.setTripData(imei, prefs.getString("cabNumber", ""));
        sendGpsData.UpdateDateTime();
        if (this.firstTime) {
            sendGpsData.setStatus("S");
            //this.firstTime = false;
        } else if(logout){
            sendGpsData.setStatus("E");
        }else {
            sendGpsData.setStatus("F");

        }
        sendGpsData.setTripData(imei, prefs.getString("cabNumber", ""));
        sendGpsData.createTripString();
        Log.v("asd", "Running - waiting");
        String str = sendGpsData.getCommandString();
        PrintWriter out = null;
        if (this.firstTime) {
            sendGpsData.sendGpsData(getApplicationContext());
            /*try {
                out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())),
                        true);
                Log.v("asd", "tt Running - waiting");
                Log.v("asd", str + "messaGE");
                out.println(str);
                this.firstTime = false;
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "error: couldnt send data:" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }catch (NullPointerException e) {
                e.printStackTrace();
                //new Thread(new ClientThread()).start();
            }*/
        }
        //    Toast.makeText(getBaseContext(),"Data Sent" + str,Toast.LENGTH_LONG).show();
        command = "E,0";
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public String getLocationName(Double latitude, Double longitude) {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Log.v("Waiting", "ADDRESS = " + addresses);

            String address = addresses.get(0).getAddressLine(1);
            Log.v("Waiting", "ADDRESS = " + address);

            return address;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void DisplayImage() {
        new AsyncTask<Void, Void, JSONObject>() { // create a new thread to process
            @Override
            protected JSONObject doInBackground(Void... params) { // what to do in the thread
                file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/tabcab/" + filename);

                if (!file.exists()) {
                    while (!(downloadTask.getStatus() == AsyncTask.Status.FINISHED)) {
                        Log.v("image downloading", "processing");
                    }
                } else {

                }

                return new JSONObject();
            }

            @Override
            protected void onPostExecute(JSONObject response) {
                if (!prefs.getString("picture", "null").equalsIgnoreCase("null")) {
                    Bitmap yourSelectedImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                    profile.setImageBitmap(yourSelectedImage);
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;


        mapHelper = new MapHelper(map);

    }


    @Override
    public void onBackPressed() {
        //moveTaskToBack(true);
        Toast.makeText(getBaseContext(),"Cannot go back", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }



}
