package in.co.datavoice.tabcabdriver.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import in.co.datavoice.tabcabdriver.R;
import in.co.datavoice.tabcabdriver.utils.DatabaseHandler;
import in.co.datavoice.tabcabdriver.utils.MyJSON;
import in.co.datavoice.tabcabdriver.utils.SendGpsData;

public class Main2Activity extends AppCompatActivity implements LocationListener, View.OnClickListener {

    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 11;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    private static final int REQUEST_CHECK_SETTINGS = 12;

    Context mContext;
    TextView tvDropLocation, tvSpeed, tvJobNo, tvCustomerName, tvKM, tvRs, tvTravelTime;
    Button btnStartStop;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private SendGpsData sendGpsData;
    private String imei;
    private LocationManager locationManager;
    private Timer timer, timer1;
    private TimerTask tt, tt1;
    private boolean TripStarted = false;
    private String TotalTripTime, strSpeed = "0 Km/h";
    private double roundDistance;
    Date StartTime, endTime, waitTime, reachedTime, tripTime;
    Long FinalTime;
    int FinalTimeMin = 0;
    int FinalTimeSec = 0;
    int FinalTimeHr = 0;
    Long extraWaitingTime;
    private String TAG = "Main2Activity";
    int seconds1 = 0, minutes1 = 0, hours1 = 0;
    private String Smsaction;
    private double finalAmount = 0;
    double distance = 0;
    private String startDateTime;
    private Location oldLocation;
    double currentDistance = 0;
    float BaseFare = 0, kmFare = 0, waitingFare = 0;
    private double amountWithTax;
    private float tax;
    private GoogleApiClient mGoogleApiClient;
    private Location newLocation;
    private MediaPlayer mMediaPlayer;
    ProgressDialog dialog;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mContext = getApplicationContext();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = prefs.edit();
        tvDropLocation = findViewById(R.id.droplocation);
        tvSpeed = findViewById(R.id.speed);
        tvJobNo = findViewById(R.id.jobno);
        tvCustomerName = findViewById(R.id.customername);
        tvKM = findViewById(R.id.kmtravel);
        tvRs = findViewById(R.id.totalamount);
        tvTravelTime = findViewById(R.id.traveltime);
        btnStartStop = findViewById(R.id.startstoptrip);
        btnStartStop.setOnClickListener(this);
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);

        setFareDetail();

        Calendar c = Calendar.getInstance();
        waitTime = c.getTime();

        PackageInfo pInfo = null;
        String version = "";
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        imei = telephonyManager.getDeviceId();
        timer = new Timer();
        timer1 = new Timer();

        sendGpsData = new SendGpsData(version);
        sendGpsData.setStatus("R");
        sendGpsData.setTripData(imei, prefs.getString("cabNumber", ""), prefs.getString("trip_id", ""), prefs.getString("date", ""));

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //noinspection ResourceType
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000,   // 1 sec
                0,  // 20 meter
                this);

        initGoogleAPIClient();//Init Google API Client
        checkPermissions();//Check Permission
        registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Register broadcast receiver to check the status of GPS
        final Handler mHandler = new Handler() {
            @SuppressLint("DefaultLocale")
            public void handleMessage(Message msg) {
                // set the distance and time when needed

                tvTravelTime.setText(TotalTripTime);
                tvKM.setText(String.format("%.2f", roundDistance));
                tvRs.setText("Rs. " + String.format("%.2f", finalAmount));
            }
        };
        // update the distance and time on the screen (needs optimizing as it runs slowly if the car goes faster
        timer.scheduleAtFixedRate(tt = new TimerTask() {
            @Override
            public void run() {
                // if the trip has started and the cab is not moving
                try {
                    String str = "";
                    sendGpsData.UpdateDateTime();
                    if (TripStarted) {
                        sendGpsData.setStatus("H");
                        sendGpsData.setAmount(finalAmount);
                    } else {
                        sendGpsData.setStatus("R");
                    }
                    sendGpsData.setImei(imei);
                    sendGpsData.createTripString();
                    sendGpsData.sendGpsData(getApplicationContext());
                    //mHandler2.obtainMessage(1).sendToTarget();

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }, 1000, 5000);//put here time 1000 milliseconds=1 second

        timer1.scheduleAtFixedRate(tt1 = new TimerTask() {
            @Override
            public void run() {

                if (TripStarted) {
                    calculateTime();
                    calculateAmount();
                    sendGpsData.setTripTime(TotalTripTime);
                    Log.v(TAG, "(process) Total TIme: " + TotalTripTime);
                    mHandler.obtainMessage(1).sendToTarget();
                }


            }
        }, 1000, 1000);

        try {
            TripStarted = prefs.getBoolean("TripStarted", false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TripStarted) {
            try {
                stop();
                distance = prefs.getFloat("distance", 0);
                roundDistance = distance / 1000;
                finalAmount = prefs.getFloat("finalAmount", 0);
                FinalTimeHr = prefs.getInt("FinalTimeHr", 0);
                FinalTimeMin = prefs.getInt("FinalTimeMin", 0);
                FinalTimeSec = prefs.getInt("FinalTimeSec", 0);


                waitTime = new Date(prefs.getLong("waitTime", 0));
                StartTime = new Date(prefs.getLong("StartTime", 0));
                Log.v(TAG, "startTime: " + StartTime);
                Smsaction = "tripStart";
                TripStarted = true;
                sendGpsData.setStatus("H");
                btnStartStop.setText(R.string.stop_trip);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            CountDownTimer cntr_aCounter = new CountDownTimer(5000, 1000) {
                public void onTick(long millisUntilFinished) {
                    play(getBaseContext(), R.raw.beep);
                }

                public void onFinish() {
                    //code fire after finish
                    stop();
                }
            };
            cntr_aCounter.start();
        }
    }

    private void setFareDetail() {

        finalAmount = BaseFare = Float.parseFloat(prefs.getString("base_fare", "0"));
        waitingFare = Float.parseFloat(prefs.getString("waiting", "0"));
        kmFare = Float.parseFloat(prefs.getString("per_km_fare", "0"));
        tax = Float.parseFloat(prefs.getString("tax", "5"));

        tvDropLocation.setText(prefs.getString("dest_location", "No Location"));
        tvCustomerName.setText(prefs.getString("cust_name", "No Name"));
      //  tvJobNo.setText("Job No.\n" + prefs.getString("trip_id", "0"));
        tvRs.setText("Rs. " + String.format("%.2f", finalAmount));

    }

    public String calculateTime() {
        long wait_time = waitTime.getTime();
        long start_time = StartTime.getTime();
        prefs.edit().putLong("waitTime", waitTime.getTime()).apply();
        // check the waiting time
        Log.v(TAG, "wait time:" + wait_time + "< start time:" + start_time);

        // there is some waiting time, find out if its > 15 mins
        extraWaitingTime = start_time - wait_time;
        Log.v(TAG, "extraWaitingTime:" + extraWaitingTime + "< 900000");
        if (extraWaitingTime < (1000 * 60 * 15)) {
            // if < 15 mins
            extraWaitingTime = 1L;
        } else {
            // convert to minutes
            extraWaitingTime = extraWaitingTime / 60000;
            extraWaitingTime = extraWaitingTime - 15;
            Log.v(TAG, "final extraWaitingTime" + extraWaitingTime);
        }
        // convert it into minutes for calculation
        Calendar c = Calendar.getInstance();
        endTime = c.getTime();
        FinalTime = endTime.getTime() - StartTime.getTime();
        Log.v(TAG, "endTime: " + endTime);
        Log.v(TAG, "difference: " + FinalTime);
        if (FinalTime > 1000) {
            FinalTimeHr = Math.round(FinalTime / (1000 * 60 * 60));
            FinalTimeMin = Math.round(FinalTime / (1000 * 60));
            if (FinalTimeMin >= 60) {
                FinalTimeMin = FinalTimeMin % 60;
            }
            FinalTimeSec = Math.round(FinalTime / 1000);
            if (FinalTimeSec >= 60) {
                FinalTimeSec = FinalTimeSec % 60;
            }
        }

        editor.putInt("FinalTimeHr", FinalTimeHr);
        editor.putInt("FinalTimeMin", FinalTimeMin);
        editor.putInt("FinalTimeSec", FinalTimeSec);
        editor.apply();
        //tvDate.setText(pad_number(FinalTimeHr) + ":" + pad_number(FinalTimeMin) + " : " + pad_number(FinalTimeSec));
        return TotalTripTime = pad_number(FinalTimeHr) + ":" + pad_number(FinalTimeMin) + ":" + pad_number(FinalTimeSec);
    }

    public String pad_number(int number) {
        if (number < 10) {
            return "0" + number;
        } else {
            return number + "";
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //strSpeed = "Speed\n" + (int)(location.getSpeed() * 3.6) + " Km/h";
        //Toast.makeText(mContext, "Accuracy : " + location.getAccuracy(), Toast.LENGTH_SHORT).show();
        Log.v(TAG, location.getAccuracy() + " Accuracy");
        sendGpsData.setLocation(location);
        sendGpsData.UpdateDateTime();
        sendGpsData.createTripString();
        newLocation = location;

        Log.v("SENDATA", "function called");

        if (TripStarted) {
            if (oldLocation == null) {
                //Toast.makeText(mContext, "Old Location is NULL", Toast.LENGTH_SHORT).show();
                oldLocation = location;
                distance = 0.0;
                editor.putFloat("distance", (float) distance).apply();
            } else {
                float temp_distance = oldLocation.distanceTo(location), temp_accuracy = location.getAccuracy();
                // ensure that the cab has moved a cirtain distance before deciding if it is really still (to compensate for the lag
                // of onLocationChanged
                if (temp_distance > temp_accuracy) // if it is not moving
                {
                    strSpeed = (int) (location.getSpeed() * 3.6) + " Km/h";
                    distance += temp_distance;
                    editor.putFloat("distance", (float) distance).apply();
                    oldLocation = location;

                    calculateTime();

                    roundDistance = distance / 1000;
                    Log.v(TAG, "KM travel " + String.format("%.2f", roundDistance) + " KM");
                    Log.v("distance set", " total: " + distance);
                } else {
                    //Toast.makeText(mContext, "Accuracy is more than distance", Toast.LENGTH_SHORT).show();
                }
            }
            try {
                sendGpsData.setLocation(location);
                sendGpsData.setDistance(distance);
                sendGpsData.UpdateDateTime();
                sendGpsData.setStatus("H");
                sendGpsData.createTripString();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*if (location.getAccuracy() <= 15) {

           *//* stringArrayList.add(location.getLatitude() + "," + location.getLongitude() + " Accuracy" + location.getAccuracy());
            adapter.notifyDataSetChanged();*//*

            //start.setEnabled(true);
            btnStartStop.setEnabled(true);
            // get the location
            if (oldLocation == null) {
                oldLocation = location;
                if(distance > 0.0){
                    //do nothing
                }else {
                    distance = 0.0;
                    editor.putFloat("distance", (float) distance).apply();
                }
            } else {
                if (TripStarted) {
                    // ensure that the cab has moved a cirtain distance before deciding if it is really still (to compensate for the lag
                    // of onLocationChanged
                    *//*if (oldLocation.distanceTo(location) <= 10) // if it is not moving
                    {
                        strSpeed = "Speed\n0 Km/h";
                        //do nothing
                    } else {*//*
                        strSpeed = "Speed\n" + (int)(location.getSpeed() * 3.6) + " Km/h";
                       *//* if((location.getSpeed() * 3.6) > 0) {*//*
                            distance += oldLocation.distanceTo(location);
                            editor.putFloat("distance", (float) distance).apply();
                        //}
                        oldLocation = location;
                    //}
                    // increment the distance

                    calculateTime();

                    roundDistance = distance / 1000;
                    Log.v(TAG, "KM travel " + String.format("%.2f", roundDistance) + " KM");
                    Log.v("distance set", " total: " + distance);

                }
            }
            // set the string with the location
            sendGpsData.setLocation(location);
            sendGpsData.UpdateDateTime();
            sendGpsData.createTripString();

            Log.v("SENDATA", "function called");
            if (TripStarted) {
                try {
                    sendGpsData.setLocation(location);
                    sendGpsData.setDistance(distance);
                    sendGpsData.UpdateDateTime();
                    sendGpsData.setStatus("H");
                    sendGpsData.createTripString();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }else{
            strSpeed = "Speed\n0 Km/h";
        }*/
        tvSpeed.setText(strSpeed);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startstoptrip) {
            if (btnStartStop.getText().toString().equalsIgnoreCase(getResources().getString(R.string.start_trip))) {
                // its started
                //stop();
                dialog.setTitle("Starting Trip");
                dialog.setMessage("Please wait");
                final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );

                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Confirm Start Trip")
                        .setMessage("Please enter the Booking number")
                        .setView(input)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String trip_id = input.getText().toString();
                                if(trip_id.equalsIgnoreCase(prefs.getString("trip_id", "0")))
                                {
                                    // start the trip
                                    tvJobNo.setText("Job No.\n" + prefs.getString("trip_id", "0"));
                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
                                    startDateTime = sdf.format(new Date());
                                    StartTime = c.getTime();
                                    prefs.edit().putLong("StartTime", StartTime.getTime()).apply();
                                    Log.v(TAG, "startTime: " + StartTime);
                                    Smsaction = "tripStart";

                                    TripStarted = true;
                                    prefs.edit().putBoolean("TripStarted", TripStarted).apply();
                                    sendGpsData.setStatus("H");
                                    Toast.makeText(getBaseContext(), "Trip Started", Toast.LENGTH_SHORT).show();
                                    //start.setText("STOP TRIP");
                                    btnStartStop.setText(R.string.stop_trip);
                                    //navigate.setEnabled(true);
                                    sendSms();

                                }
                                else
                                {
                                    input.setError("Incorrect Trip id");
                                    Toast.makeText(getBaseContext(),"ERROR: incorrect booking id given",Toast.LENGTH_LONG).show();

                                }
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();


            } else if (btnStartStop.getText().toString().equalsIgnoreCase(getResources().getString(R.string.stop_trip))) {
                dialog.setTitle("Ending Trip");
                dialog.setMessage("Please wait");
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Confirm Stop Trip")
                        .setMessage("Are you sure you want to stop trip?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Smsaction = "tripEnd";
                                if(oldLocation != null && newLocation != null) {
                                    distance += oldLocation.distanceTo(newLocation);
                                }
                                TripStarted = false;
                                btnStartStop.setEnabled(false);
                                prefs.edit().putBoolean("TripStarted", TripStarted).apply();
                                sendFareDetails();


                            }

                        })
                        .setNegativeButton("No", null)
                        .show();


                //        fareAmount= "" ;// send the fare amount (coming soon)
            }
        }
    }

    private void sendFareDetails() {
        new AsyncTask<Void, Void, JSONObject>() { // create a new thread to process

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog.show();
            }

            @Override
            protected JSONObject doInBackground(Void... params) { // what to do in the thread
                MyJSON test = new MyJSON(getApplicationContext());
                String Json1 = "";
                try {
                    HttpResponse Json2;

                    amountWithTax = finalAmount + (finalAmount * (tax / 100));
                    Json1 = test.CreateFare(String.format("%.2f", distance / 1000) + "", TotalTripTime, prefs.getString("trip_id", ""), String.format("%.2f", amountWithTax), imei);
                    if (isConnected()) {
                        Json2 = test.SendJSON("/InsertPaymentDetails.php", Json1);
                    } else {
                        DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
                        databaseHandler.addTripJson(prefs.getString("trip_id", ""), databaseHandler.getCurrentTimeStamp(), Json1, "/InsertPaymentDetails.php", "N");
                        return new JSONObject();
                    }
                    Log.v(TAG, "SENDING" + Json1);

                     // convert the response into a JSON object
                    JSONObject Json3 = test.GetJson(Json2);
                    Log.v(TAG, "Recived: " + Json3.toString());
                    return Json3;


                } catch (UnsupportedEncodingException | JSONException | NullPointerException e) {
                    Log.v(TAG, e.toString());
                    DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
                    databaseHandler.addTripJson(prefs.getString("trip_id", ""), databaseHandler.getCurrentTimeStamp(), Json1, "/InsertPaymentDetails.php", "N");
                    return new JSONObject();
                }
            }

            @Override
            protected void onPostExecute(JSONObject response) {
                // mobile.setText(response);
                // JSONData = response;
                if(dialog.isShowing())
                    dialog.dismiss();
                sendSms();
            }

        }.execute(null, null, null);
    }

    private void sendSms() {
        new AsyncTask<Void, Void, JSONObject>() { // create a new thread to process

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog.show();
            }

            @Override
            protected JSONObject doInBackground(Void... params) { // what to do in the thread
                MyJSON test = new MyJSON(getApplicationContext());
                String Json1 = "";
                try {

                    if (Smsaction.equalsIgnoreCase("tripStart")) {
                        Json1 = test.CreateStartsms(Smsaction, prefs.getString("cust_number", ""), prefs.getString("cust_name", ""), prefs.getString("trip_id", ""), imei);
                    } else {
                        amountWithTax = finalAmount + (finalAmount * (tax / 100));
                        Json1 = test.CreateStopsms(Smsaction, prefs.getString("cust_number", ""), prefs.getString("cust_name", ""),
                                prefs.getString("trip_id", ""), String.format("%.2f", amountWithTax), imei, String.format("%.2f", distance / 1000), TotalTripTime, extraWaitingTime + "");

                    }
                    HttpResponse Json2;
                    if (isConnected()) {
                        Json2 = test.SendJSON("/sendSms.php", Json1);
                    } else {
                        DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
                        databaseHandler.addTripJson(prefs.getString("trip_id", ""), databaseHandler.getCurrentTimeStamp(), Json1, "/sendSms.php", "N");
                        return new JSONObject();
                    }
                    Log.v(TAG, "SENDING" + Json1);

                     // convert the response into a JSON object
                    JSONObject Json3 = test.GetJson(Json2);
                    Log.v(TAG, "Recived: " + Json3.toString());
                    return Json3;


                } catch (UnsupportedEncodingException | JSONException | NullPointerException e) {
                    Log.v(TAG, e.toString());
                    DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
                    databaseHandler.addTripJson(prefs.getString("trip_id", ""), databaseHandler.getCurrentTimeStamp(), Json1, "/sendSms.php", "N");
                    return new JSONObject();
                }

            }

            @Override
            protected void onPostExecute(JSONObject response) {
                // mobile.setText(response);
                // JSONData = response;
                try {
                    if(dialog.isShowing())
                        dialog.dismiss();
                    //JSONArray data = responsedata.getJSONArray("data");

                    //JSONObject fields = data.getJSONObject(0);
                    //  Log.v("the response is: ", "something1" + fields.toString());
                    if (response.has("status")) {
                        if (response.getString("status").equalsIgnoreCase("OK") && Smsaction.equalsIgnoreCase("tripEnd")) // if there are children
                        {


                            Intent I = new Intent(getBaseContext(), Billing.class);
                            I.putExtra("totalBill", finalAmount);
                            I.putExtra("amountWithTax", amountWithTax);
                            I.putExtra("distance", String.format("%.2f", distance / 1000) + "");
                            I.putExtra("tripTime", TotalTripTime);
                            I.putExtra("paymentMode", "Cash");
                            I.putExtra("TotalTripTime", TotalTripTime);


                            startActivity(I);
                            tt.cancel();
                            tt1.cancel();
                            timer.cancel();
                            timer.purge();
                            timer1.cancel();
                            timer1.purge();

                            locationManager.removeUpdates(Main2Activity.this);
                            //finish();


                        }
                    } else {
                        if (Smsaction.equalsIgnoreCase("tripEnd")) {
                            Intent I = new Intent(getBaseContext(), Billing.class);
                            I.putExtra("totalBill", finalAmount);
                            I.putExtra("amountWithTax", amountWithTax);
                            I.putExtra("distance", String.format("%.2f", distance / 1000) + "");
                            I.putExtra("tripTime", TotalTripTime);
                            I.putExtra("paymentMode", "Cash");
                            I.putExtra("TotalTripTime", TotalTripTime);


                            startActivity(I);
                            tt.cancel();
                            tt1.cancel();
                            timer.cancel();
                            timer.purge();
                            timer1.cancel();
                            timer1.purge();

                            locationManager.removeUpdates(Main2Activity.this);
                        }
                        //  Toast.makeText(getApplicationContext(), "Error: could not send sms", Toast.LENGTH_LONG).show();
                        Log.v(TAG, "error");
                    }
                } catch (Exception e) {
                    Log.v(TAG, e.toString());
                    if(dialog.isShowing())
                        dialog.dismiss();
                    if (Smsaction.equalsIgnoreCase("tripEnd")) {
                        Intent I = new Intent(getBaseContext(), Billing.class);
                        I.putExtra("totalBill", finalAmount);
                        I.putExtra("amountWithTax", amountWithTax);
                        I.putExtra("distance", String.format("%.2f", distance / 1000) + "");
                        I.putExtra("tripTime", TotalTripTime);
                        I.putExtra("paymentMode", "Cash");
                        I.putExtra("TotalTripTime", TotalTripTime);


                        startActivity(I);
                        tt.cancel();
                        tt1.cancel();
                        timer.cancel();
                        timer.purge();
                        timer1.cancel();
                        timer1.purge();

                        locationManager.removeUpdates(Main2Activity.this);
                    }
                }

            }
        }.execute(null, null, null);
    }


    public void calculateAmount() {
        currentDistance = distance;
        finalAmount = ((currentDistance / 1000) * kmFare);
        prefs.edit().putFloat("finalAmount", (float) finalAmount).apply();
        Log.v(TAG, "TOTAL AMOUNT: " + finalAmount);
        finalAmount = Math.round(finalAmount);
        Log.v("currentDistance:", (currentDistance / 1000) + "*" + kmFare);
    }

    public boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    /* Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(Main2Activity.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(Main2Activity.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(Main2Activity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(Main2Activity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(Main2Activity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //updateGPSStatus("GPS is Enabled in your device");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(Main2Activity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        try {
                            status.startResolutionForResult(Main2Activity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.e("Settings", "Result OK");
                        //updateGPSStatus("GPS is Enabled in your device");
                        //startLocationUpdates();
                        break;
                    case RESULT_CANCELED:
                        Log.e("Settings", "Result Cancel");
                        checkPermissions();
                        //updateGPSStatus("GPS is Disabled in your device");
                        break;
                }
                break;
        }
    }

    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device");
                    //updateGPSStatus("GPS is Enabled in your device");
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
                    //updateGPSStatus("GPS is Disabled in your device");
                    Log.e("About GPS", "GPS is Disabled in your device");
                }

            }
        }
    };

    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showSettingDialog();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000,   // 1 sec
                0,  // 20 meter
                this);
        registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Register broadcast receiver to check the status of GPS
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Unregister receiver on destroy
        if (gpsLocationReceiver != null)
            unregisterReceiver(gpsLocationReceiver);
    }
    @Override
    public void onBackPressed() {
        //moveTaskToBack(true);
        //Toast.makeText(getBaseContext(),"Cannot go back",Toast.LENGTH_SHORT).show();
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }else {
            dialog.setTitle("Ending Trip");
            dialog.setMessage("Please wait");
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Confirm Stop Trip")
                    .setMessage("Are you sure you want to stop trip?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Smsaction = "tripEnd";
                            if (oldLocation != null && newLocation != null) {
                                distance += oldLocation.distanceTo(newLocation);
                            }
                            TripStarted = false;
                            btnStartStop.setEnabled(false);
                            prefs.edit().putBoolean("TripStarted", TripStarted).apply();
                            sendFareDetails();


                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }

    public void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        //thread.interrupt();
        //Thread.currentThread().interrupted();
    }

    public void play(Context c, int rid) {
        stop();

        mMediaPlayer = MediaPlayer.create(c, rid);
        //mMediaPlayer.setLooping(true);
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stop();
            }
        });

        mMediaPlayer.start();
    }

}
