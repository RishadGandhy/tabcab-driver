package in.co.datavoice.tabcabdriver.services;

/*
 * Created by Rishad on 12/12/2014.
 */

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import in.co.datavoice.tabcabdriver.R;
import in.co.datavoice.tabcabdriver.activities.Main2Activity;
import in.co.datavoice.tabcabdriver.activities.Waiting;

public class GcmIntentService extends IntentService {
//    int mId = 542321;
    SharedPreferences prefs;
    String TAG = "PUSHMESSAGE";
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;
    NotificationManager mNotificationManager;
    int notificationTime = 30;
    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString());
                // If it's a regular GCM message, do some work.

            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                Log.v("GCM", extras.toString());

              //   MapsActivity update = new MapsActivity();
                Log.i("GCM", "" +
                        " work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
             //   Log.v("GCM",extras.getString("m").toString());
                try {
                    JSONObject trip_details = new JSONObject(extras.getString("message"));
                    if(trip_details.getString("type").equalsIgnoreCase("cancel"))
                    {
                        Intent i = new Intent(getBaseContext(),Waiting.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                    else {
                        Log.v(TAG,"Trip details : " + trip_details);
                        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("trip_id", trip_details.getString("trip_id"));
                        editor.putString("dest_location", trip_details.getString("dest_location"));
                        editor.putString("cust_name", trip_details.getString("cust_name"));
                        editor.putString("fare", trip_details.getString("fare"));
                        editor.putString("date", trip_details.getString("date"));
                        editor.putString("base_fare", trip_details.getString("base_fare"));
                        editor.putString("waiting", trip_details.getString("waiting"));
                        editor.putString("per_km_fare", trip_details.getString("per_km_fare"));
                        editor.putString("tax", trip_details.getString("tax").contains("%") ? trip_details.getString("tax").replace("%","") : trip_details.getString("tax"));

                        editor.apply();
                        Log.v("intent", trip_details.getString("cust_name"));

                        //sendNotification("Address:  " + trip_details.getString("location") + "\nTime: " + trip_details.getString("date" ));
                        GcmBroadcastReceiver.completeWakefulIntent(intent);
                        Intent i = new Intent(getBaseContext(), Main2Activity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Waiting.timertt.cancel();
                        Waiting.timertt.purge();
                        startActivity(i);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //sendNotification(extras.getString("m"));

                Log.v("GCM", "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.

    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /*Intent accept = new Intent(getBaseContext(),Main2Activity.class);
        accept.putExtra("accept","y");
        PendingIntent AIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), accept, 0);
        Intent decline = new Intent(getBaseContext(),BidAccept.class);
        decline.putExtra("accept","n");
        PendingIntent DIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), decline, 0);*/
        long[] vibrate = { 0, 100, 200, 300 };

        mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("New Trip Request")
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setVibrate(vibrate)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg));
        Intent resultIntent = new Intent(this, Main2Activity.class);
        resultIntent.putExtra("accept","y");
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(Main2Activity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.

        //mNotificationManager.notify(mId,mBuilder.build());
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        int incr;
                        // Do the "lengthy" operation 20 times
                        for (incr = 0; incr <= notificationTime; incr+=5) { // change to 60 later
                            // Sets the progress indicator to a max value, the
                            // current completion percentage, and "determinate"
                            // state
                            mBuilder.setProgress(notificationTime, incr, false);
                            // Displays the progress bar for the first time.
                            mNotifyManager.notify(0, mBuilder.build());

                            // Sleeps the thread, simulating an operation
                            // that takes time
                            try {
                                // Sleep for 5 seconds
                                Thread.sleep(5*1000);
                            } catch (InterruptedException e) {
                                Log.d(TAG, "sleep failure");
                            }
                        }

                        mNotificationManager.cancelAll();

                    }
                }
// Starts the thread by calling the run() method in its Runnable
        ).start();


    }
}