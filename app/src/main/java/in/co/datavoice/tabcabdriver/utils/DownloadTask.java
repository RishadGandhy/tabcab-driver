package in.co.datavoice.tabcabdriver.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Rishad on 8/3/2015.
 */
    public class DownloadTask extends AsyncTask<String, Integer, String> {

    private Context context;
    private PowerManager.WakeLock mWakeLock;
    private String filename;
    private boolean showDialog = true;
    ProgressDialog mProgressDialog;

    public DownloadTask(Context context, String filename, ProgressDialog mProgressDialog) {
        this.context = context;
        this.filename = "photo.jpg";
        this.mProgressDialog = mProgressDialog;
        this.showDialog = true;

    }
    public DownloadTask(String filename, boolean showDialog)
    {
        this.filename = filename;
        this.showDialog = showDialog;
    }
    @Override
    protected String doInBackground(String... sUrl) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl[0]);
            Log.v("DT ERROR",url.toString());
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            File yourFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/tabcab/" + filename);
            if(!yourFile.exists()) {
                yourFile.createNewFile();
            }

            output = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + "/tabcab/" + filename);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            Log.v("DT",e.getStackTrace().toString());

            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        //   PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        //   mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
        //          getClass().getName());
        //  mWakeLock.acquire();
        if(showDialog) {
            mProgressDialog.show();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // if we get here, length is known, now set indeterminate to false
        if(showDialog) {
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        //mWakeLock.release();
        if(showDialog) {
            mProgressDialog.dismiss();
            if (result != null) {
                Toast.makeText(context, "Error : Please Ensure you have a working internet connection and try again", Toast.LENGTH_LONG).show();
                Log.v("Download", result);
            }
            else
            {
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            if (result != null) {
                Log.v("Download failed: ", result);
            }
            else
            {
                Log.v("Downloaded: ", "file downloaded");
            }

        }

    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}