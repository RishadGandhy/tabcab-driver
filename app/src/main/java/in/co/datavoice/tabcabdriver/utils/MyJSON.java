package in.co.datavoice.tabcabdriver.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by Rishad on 12/15/2014.
 * All functions using Json encode and decode must be passed through this class
 * also used to send and get requests
 */
public class MyJSON extends JSONObject {
    private final Context context;
    JSONObject jsonobj = new JSONObject(); // declared locally so that it destroys after serving its purpose
    private SharedPreferences prefs;

    public MyJSON(Context applicationContext) {
        this.context = applicationContext;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);

    }

    // Create the Json String basedF on an id
    public String CreateJSON(String data) {

        try {
            // adding some keys
            Log.v("the data= ", data);
            jsonobj.put("id", data);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }
    public String CreateJSON(String[] data) {

        try {
            for(int i=0;i<data.length;i++)
            {
                    Log.v("the data= ", data[i]);
                jsonobj.put("id" + i, data[i]);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }

    public String CreateJSON2(String driver_id, String device_id, String latitude, String longitude, String driver_name, String cab_id,
                              String pickup_location_lat, String pickup_location_long, String trip_id, String activity_code,
                              String response_code , String bid_id ) {
        try {
            // adding some keys
           // Log.v("the data= ", mobno + " ; " + regid);
            jsonobj.put("driver_id", driver_id);
            jsonobj.put("device_id", device_id);
            jsonobj.put("latitude", latitude);
            jsonobj.put("longitude", longitude);
            jsonobj.put("driver_name", driver_name);
            jsonobj.put("cab_id", cab_id);
            jsonobj.put("pickup_location_lat", pickup_location_lat);
            jsonobj.put("pickup_location_long", pickup_location_long);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("activity_code", activity_code);
            jsonobj.put("response_code", response_code);
            jsonobj.put("bid_id", bid_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }
    public String CreateJSON2(String mobno, String regid) {
        try {
            // adding some keys
            Log.v("the data= ", mobno + " ; " + regid);
            jsonobj.put("id1", mobno);
            jsonobj.put("id2", regid);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }
    public String CreateJSONLogin(String mobno, String password, String imei) {
        try {
            // adding some keys
            //Log.v("the data= ", mobno + " ; " + regid);
            jsonobj.put("id0", mobno);
            jsonobj.put("id1", password);
            jsonobj.put("id2", imei);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }
    public String CreateJSON2(String imei, String Lat, String Long) {
        try {
            // adding some keys
            jsonobj.put("device_id", imei);
            jsonobj.put("latitude", Lat);
            jsonobj.put("longitude", Long);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }
    public String CreateBidAction(String driver_id, String driver_name, String cab_id, String trip_id, String action,
                                  String imei, String Cabnumber, String latitude, String longitude) {
        try {
            // adding some keys
            jsonobj.put("driver_id", driver_id);
            jsonobj.put("driver_name", driver_name);
            jsonobj.put("cab_id", cab_id);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("action", action);
            jsonobj.put("cab_number", Cabnumber);
            jsonobj.put("device_id", imei);
            jsonobj.put("latitude", latitude);
            jsonobj.put("longitude", longitude);



        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }
    public String CreateReachedsms(String message_type, String number, String name, String trip_id, String cabNumber, String pickupDistance, String imei, String latitude, String longitude, String date)
    {
            String[] datetime = date.split(" ");
            String time =datetime[1];

        try {
            // adding some keys
            jsonobj.put("message_type", message_type);
            jsonobj.put("number", number);
            jsonobj.put("name", name);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("cabNumber", cabNumber);
            jsonobj.put("proximity", pickupDistance);
            jsonobj.put("imei", imei);
            jsonobj.put("latitude", latitude);
            jsonobj.put("longitude", longitude);
            jsonobj.put("time",time);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }
    public String CreateStartsms(String message_type, String number, String name, String trip_id, String Proximity, String imei, String latitude, String longitude, String startSpeedometer, boolean intercity)
    {
        try {
            // adding some keys
            jsonobj.put("message_type", message_type);
            jsonobj.put("number", number);
            jsonobj.put("name", name);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("proximity", Proximity);
            jsonobj.put("imei", imei);
            jsonobj.put("latitude", latitude);
            jsonobj.put("longitude", longitude);
            jsonobj.put("startSpeedometer", startSpeedometer);
            jsonobj.put("intercity", intercity);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }
    public String CreateStartsms(String smsaction, String cust_number, String cust_name, String trip_id, String imei) {
        try {
            // adding some keys
            jsonobj.put("message_type", smsaction);
            jsonobj.put("number", cust_number);
            jsonobj.put("name", cust_name);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("imei", imei);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }

    public String CreateStopsms(String message_type, String number, String name, String trip_id, String amount, String Proximity, String imei, String latitude, String longitude, String distance, String TotalTripTime, String extraWaitingTime, String stopSpeedometer, boolean intercity)
    {
        try {
            // adding some keys
            jsonobj.put("message_type", message_type);
            jsonobj.put("number", number);
            jsonobj.put("name", name);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("amount", amount);
            jsonobj.put("proximity", Proximity);
            jsonobj.put("imei", imei);
            jsonobj.put("latitude", latitude);
            jsonobj.put("longitude", longitude);
            jsonobj.put("distance", distance);
            jsonobj.put("TotalTripTime", TotalTripTime);
            jsonobj.put("extraWaitingTime", extraWaitingTime);
            jsonobj.put("stopSpeedometer", stopSpeedometer);
            jsonobj.put("intercity", intercity);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }

    public String CreateStopsms(String smsaction, String cust_number, String cust_name, String trip_id, String amount, String imei, String distance, String totalTripTime, String extraWaitingTime) {
        try {
            // adding some keys
            jsonobj.put("message_type", smsaction);
            jsonobj.put("number", cust_number);
            jsonobj.put("name", cust_name);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("amount", amount);
            jsonobj.put("imei", imei);
            jsonobj.put("distance", distance);
            jsonobj.put("TotalTripTime", totalTripTime);
            jsonobj.put("extraWaitingTime", extraWaitingTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }

    public String CreateFare(String distance, String waitTime, String fare, String trip_id, String amount, String tax, String imei, String latitude, String longitude, String dateTime, String extraWaitingTime, String toll, String parking)
    {
        try {
            // adding some keys
            jsonobj.put("distance", distance);
            jsonobj.put("waitTime", waitTime);
            jsonobj.put("name", fare);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("amount", amount);
            jsonobj.put("tax", tax);
            jsonobj.put("imei", imei);
            jsonobj.put("latitude", latitude);
            jsonobj.put("longitude", longitude);
            jsonobj.put("dateTime", dateTime);
            jsonobj.put("toll", toll);
            jsonobj.put("parking", parking);
            jsonobj.put("extraWaitingTime", extraWaitingTime);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }

    public String CreateFare(String distance, String totalTripTime, String trip_id, String amount, String imei) {
        try {
            // adding some keys
            jsonobj.put("distance", distance);
            jsonobj.put("totalTripTime", totalTripTime);
            jsonobj.put("trip_id", trip_id);
            jsonobj.put("amount", amount);
            jsonobj.put("imei", imei);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }

    public String CreateJSONReports(String imei) {
        try {
            // adding some keys

            jsonobj.put("imei", imei);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }

// send a Json object to the given URL
    public HttpResponse SendJSON(String url, String jsonobj) throws UnsupportedEncodingException {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        //url =  "http://mumairportapps.tabcab.in:4444/tab_airport_system_new/app_api" + url;
        //url = prefs.getString("url","http://mumairportapps.tabcab.in:4444/tab_airport_system_new/app_api") + url;
        url = "http://mumairportapps.tabcab.in:4444/tab_airport_system_new/app_api" + url;
        HttpPost httppostreq =   new HttpPost(url);
        StringEntity se = new StringEntity(jsonobj);
        se.setContentType("application/json;charset=UTF-8");
        se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
        httppostreq.setEntity(se);
        HttpResponse httpresponse;
        try {
            httpresponse = httpclient.execute(httppostreq);
            return httpresponse;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // extract the JSON object from the response
    public JSONObject GetJson(HttpResponse httpresponse) throws JSONException {
        HttpEntity resultentity = httpresponse.getEntity();
        InputStream inputstream = null;
        try {
            inputstream = resultentity.getContent();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Header contentencoding = httpresponse.getFirstHeader("Content-Encoding");
        String resultstring = convertStreamToString(inputstream);
        Log.v("json", resultstring);

        JSONObject recvdjson = new JSONObject(resultstring);
        return recvdjson;
    }

    private String convertStreamToString(InputStream is) {
        String line = "";
        StringBuilder total = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        try {
            while ((line = rd.readLine()) != null) {
                total.append(line);
            }
        } catch (Exception e) {
            //Toast.makeText(, "Stream Exception", Toast.LENGTH_SHORT).show();
        }
        return total.toString();
    }


    public String CreateSendGpsData(String header, String packetType, String imei, String taxiNo, String gps, String date,
                                    String time, String latitude, String latitudeDirection, String longitude,
                                    String longitudeDirection, String speed, String bearing, String status, String doorAlarm,
                                    String panicAlarm, String battery, String batteryAlert, String batteryType,
                                    String signalStrength, String s, String rateMode, String ack, String rateModeAmount,
                                    String softwareVersion, String checksum, String dataPacket, String tripId, String distance, String totalTripTime) {
        try {
            // adding some keys

            jsonobj.put("header", header);
            jsonobj.put("packetType", packetType);
            jsonobj.put("imei", imei);
            jsonobj.put("taxiNo", taxiNo);
            jsonobj.put("gps", gps);
            jsonobj.put("date", date);
            jsonobj.put("time", time);
            jsonobj.put("latitude", latitude);
            jsonobj.put("latitudeDirection", latitudeDirection);
            jsonobj.put("longitude", longitude);
            jsonobj.put("longitudeDirection", longitudeDirection);
            jsonobj.put("speed", speed);
            jsonobj.put("bearing", bearing);
            jsonobj.put("status", status);
            jsonobj.put("doorAlarm", doorAlarm);
            jsonobj.put("panicAlarm", panicAlarm);
            jsonobj.put("battery", battery);
            jsonobj.put("batteryAlert", batteryAlert);
            jsonobj.put("batteryType", batteryType);
            jsonobj.put("signalStrength", signalStrength);
            jsonobj.put("s", s);
            jsonobj.put("rateMode", rateMode);
            jsonobj.put("ack", ack);
            jsonobj.put("rateModeAmount", rateModeAmount);
            jsonobj.put("softwareVersion", softwareVersion);
            jsonobj.put("checksum", checksum);
            jsonobj.put("dataPacket", dataPacket);
            jsonobj.put("tripId", tripId);
            jsonobj.put("distance", distance);
            jsonobj.put("totalTripTime", totalTripTime);



        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {

            return jsonobj.toString();
        }
    }

}

