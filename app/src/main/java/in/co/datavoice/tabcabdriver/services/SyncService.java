package in.co.datavoice.tabcabdriver.services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import in.co.datavoice.tabcabdriver.utils.DatabaseHandler;
import in.co.datavoice.tabcabdriver.utils.MyJSON;
import in.co.datavoice.tabcabdriver.utils.TripData;

/**
 * Created by Admin on 01-02-2018.
 */

public class SyncService extends Service {
    private String TAG = "SyncService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Handler mHandler;
    // default interval for syncing data
    public static final long DEFAULT_SYNC_INTERVAL = 30 * 1000;

    // task to be run here
    private Runnable runnableService = new Runnable() {
        @Override
        public void run() {
            syncData();
            // Repeat this runnable code block again every ... min
            mHandler.postDelayed(runnableService, 600000);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
        // Execute a runnable task as soon as possible
        mHandler.post(runnableService);
    }

    /*@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create the Handler object
        mHandler = new Handler();
        // Execute a runnable task as soon as possible
        mHandler.post(runnableService);

        return START_STICKY;
    }*/

    private synchronized void syncData() {
        // call your rest service here
        DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
        TripData[] tripData = databaseHandler.getUnUpdatedTripData();
        if(tripData != null) {
            for (TripData aTripData : tripData) {
                sendData(aTripData.getJson(), aTripData.getTrip_id(), aTripData.getUrl());
            }
        }
    }

    private void sendData(final String Json1,final String trip_id, final String url) {
        new AsyncTask<Void, Void, JSONObject>() {
            public DatabaseHandler databaseHandler; // create a new thread to process

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                databaseHandler = new DatabaseHandler(getApplicationContext());
            }

            @Override
            protected JSONObject doInBackground(Void... params) { // what to do in the thread
                MyJSON test = new MyJSON(getApplicationContext());

                try {
                    HttpResponse Json2;

                        Json2 = test.SendJSON(url, Json1);

                    Log.v(TAG,"SENDING" + Json1);

                    try { // convert the response into a JSON object
                        JSONObject Json3 = test.GetJson(Json2);
                        Log.v(TAG,"Recived: " +  Json3.toString());
                        return Json3;

                    }catch (NullPointerException e)
                    {
                        Log.v(TAG, e.toString());
                    }

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
                return new JSONObject();
            }

            @Override
            protected void onPostExecute(JSONObject response) {

                try {
                    if (response.getString("status").equalsIgnoreCase("OK")) {
                        databaseHandler.update_trip_update_status(trip_id,databaseHandler.getCurrentTimeStamp());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }
}

